<?php

use yii\db\Migration;

class m130524_201442_user extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique(),
            'password' => $this->string(),
            'email' => $this->string()->unique(),
            'is_admin' => $this->integer()->defaultValue(0),
            'date' => $this->date(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
