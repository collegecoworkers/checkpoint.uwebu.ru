<?php

use yii\db\Migration;

class m171004_101859_check extends Migration
{
    public function safeUp()
    {
        $this->createTable('check', [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp()->notNull(),
            'status' => $this->integer(),
            'user_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%check}}');
    }
}
