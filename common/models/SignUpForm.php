<?php
namespace common\models;

use yii\base\Model;
use common\models\User;

class SignUpForm extends Model {
	public $username;
	public $email;
	public $password;
	
	public function rules() {
		return [
			[['username','email','password'],'required'],
			['email','email'],
			['email','unique','targetClass'=>'common\models\User'],
			['password','string']
		];
	}

	public function signup() {
		$user = new User();
		$user->username = $this->username;
		$user->email = $this->email;
		$user->is_admin = 0;
		$user->setPassword($this->password);
		return $user->save(); //вернет true или false
	}
}