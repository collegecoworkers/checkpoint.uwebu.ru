<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use common\models\Folder;

class Check extends ActiveRecord {

	public static function tableName() {
		return '{{%check}}';
	}

	public function rules() {
		return [
			[['status'], 'integer'],
		];
	}

	public function attributeLabels() {
		return [
			'status' => 'Статус',
			'date' => 'Дата',
			'user_id' => 'Пользователь',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function saveCheck(){
		return $this->save();
	}

	public function getStatus(){
		switch ($this->status) {
			case 1: return 'На предприятии';
			case 0: return 'Нет на предприятии';
			default: return 'Ошибка';
		}
	}

	public static function allStatuses(){
		$statuses = [];
		$statuses[0] = 'Нет на предприятии';
		$statuses[1] = 'На предприятии';
		return $statuses;
	}

}
