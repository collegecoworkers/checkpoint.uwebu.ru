<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {

	public static function tableName() {
		return '{{%user}}';
	}

	public function rules() {
		return [
			
		];
	}

	public function attributeLabels() {
		return [
			'username' => 'Логин',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public static function isAdmin($id) {
		return static::findOne(['id' => $id])['is_admin'] == 1;
	}

	public static function findByUsername($username) {
		return static::findOne(['username' => $username]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password);
	}

	public function setPassword($password) {
		$this->password = Yii::$app->security->generatePasswordHash($password);
	}

	public static function findIdentityByAccessToken($token, $type = null) {
	}
	
	public function getAuthKey() {
	}
	
	public function validateAuthKey($authKey) {
	}

}
