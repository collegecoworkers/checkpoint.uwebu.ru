<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\models\Check;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => ['class' => AccessControl::className(),
			'rules' => [
				[
					'actions' => [
						'signup',
						'login',
						'error'
					],
					'allow' => true,
				],
				[
					'actions' => [
						'logout',
						'index',
						'add-project',
						'update-project',
						'delete-project',
						'project',
						'down',
						'create',
						'update',
						'delete',
					],
					'allow' => true,
					'roles' => ['@'],
				],
			],
		],
	];
}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		if($this->theAdmin()){
			$model = User::find();
			return $this->render('index', [
				'model' => $model
			]);
		} else {
			$this->layout = 'check';

			$chech_record = Check::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->one();
			$model = new Check();
			$not_has = false;

			if (Yii::$app->request->isPost) {
				$model->status = $chech_record->status == 1 ? 0 : 1;
				$model->user_id = Yii::$app->user->id;
				$model->save();
				return $this->goHome();
			}

			return $this->render('check-point', [
				'model' => $model,
				'status' => $chech_record->status == 1 ? 0 : 1,
			]);
		}
	}

	public function actionCheck($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();
		$project = Check::find()->where(['id' => $id])->one();
		$model = Purpose::find()->where(['project_id' => $id]);
		$done = Purpose::find()->where(['project_id' => $id, 'status' => 1])->count();
		$all = $model->count();
		return $this->render('project', [
			'project' => $project,
			'model' => $model,
			'done' => $done,
			'all' => $all,
		]);
	}

	public function actionCreate($id) {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Purpose();

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Purpose']['name'];
			$model->desc = Yii::$app->request->post()['Purpose']['desc'];
			$model->status = Yii::$app->request->post()['Purpose']['status'];
			$model->project_id = $id;
			$model->date = date('Y-m-d');
			$model->save();
			return $this->redirect(['/site/project', 'id' => $id]);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Purpose::allStatuses(),
			'projects' => $this->getChecks(),
		]);
	}

	public function actionUpdate($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Purpose::findIdentity($id);

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Purpose']['name'];
			$model->desc = Yii::$app->request->post()['Purpose']['desc'];
			$model->status = Yii::$app->request->post()['Purpose']['status'];
			$model->save();
			return $this->redirect(['/site/project', 'id' => $model->project_id]);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Purpose::allStatuses(),
			'projects' => $this->getChecks(),
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Purpose::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionAddCheck() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Check();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Check']['title'];
			$model->desc = Yii::$app->request->post()['Check']['desc'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-project', [
			'model' => $model,
		]);
	}

	public function actionUpdateCheck($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Check::findOne($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Check']['title'];
			$model->desc = Yii::$app->request->post()['Check']['desc'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-project', [
			'model' => $model,
		]);
	}

	public function actionDeleteCheck($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Check::findOne($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				Yii::$app->user->login($user);
				return $this->goHome();
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function theAdmin(){
		return User::isAdmin(Yii::$app->user->id);
	}

}
