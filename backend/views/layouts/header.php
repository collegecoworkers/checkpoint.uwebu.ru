<?php
use yii\helpers\Html;
?>
<header class="main-header">
	<!-- Logo -->
	<a href="/" class="logo" style="">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span><b><?=$this->title?></b></span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
	<?php if(isset($is_check) && $is_check): ?>
			<span class="sr-only">Toggle navigation</span>
			<style>
				.logo{width: 50% !important;}
				nav.navbar{width: 50%!important;float: right!important;margin: 0!important;}
				.content-wrapper, .skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side{background-color: #eee!important;}
			</style>
	<?php else: ?>
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
	<?php endif ?>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="/" class="dropdown-toggle" data-toggle="dropdown">
						<?= Html::img('@web/img/user2-160x160.jpg', ['class' => 'user-image', 'alt'=>'User Image']) ?>
						<span class="hidden-xs"><?= \Yii::$app->user->identity->username ?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<?= Html::img('@web/img/user2-160x160.jpg', ['class' => 'img-circle', 'alt'=>'User Image']) ?>
							<p>
								<?= \Yii::$app->user->identity->username ?> - Вы
								<small></small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-right">
								<a href="/admin/site/logout" class="btn btn-default btn-flat">Выход</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
