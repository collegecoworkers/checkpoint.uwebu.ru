<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Project;

?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<?= Html::img('@web/img/user2-160x160.jpg', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
			</div>
			<div class="pull-left info">
				<p><?= \Yii::$app->user->identity->username ?></p>
				<a href=""><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php // var_dump($project) ?>
		<?=
			Menu::widget(
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => [
						['label' => 'Меню', 'options' => ['class' => 'header']],
						[
							'label' => 'Главная',
							'icon' => 'fa fa-home', 
							'url' => ['/'],
							'active' => $this->context->route == 'site/index'
						],
					],
				]
			)
		?>
		
	</section>
	<!-- /.sidebar -->
</aside>
