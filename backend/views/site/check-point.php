<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;

$this->title = Yii::t('app', 'Контрольно-пропускной пункт');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-md-12">
	<div class="jumbotron vertical-center">
		<div class="container">
			<?php $form = ActiveForm::begin(); ?>
			<?php if($status == 0): ?>
				<?= Html::submitButton(Yii::t('app', 'Ухожу с работы'), ['class' =>  'btn btn-danger', 'style' => 'width: 200px; height: 50px;']) ?>
			<?php else: ?>
				<?= Html::submitButton(Yii::t('app', 'Пришел на работу'), ['class' =>  'btn btn-success', 'style' => 'width: 200px; height: 50px;']) ?>
			<?php endif ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

<style>
.vertical-center {
	min-height: calc(100vh - 100px);
	text-align: center; display: flex; align-items: center;
}

</style>