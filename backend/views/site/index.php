<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\Check;

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
		'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

			<div class="contact-index">
				<br>
				<?php
				echo GridView::widget([
					'dataProvider' => $dataProvider,
					'layout' => "{items}\n{pager}",
					'columns' => [
						'username',
						[
							'label' => 'Статус',
							'attribute' => 'status',
							'value' => function($data){
								if($data->isAdmin($data->id))
									return 'admin';
								return (Check::find()->where(['user_id' => $data->id])->orderBy(['id' => SORT_DESC])->one())->getStatus();
							}
						],
					],
				]);
				?>

			</div>

		</div>
	</div>
</div>
